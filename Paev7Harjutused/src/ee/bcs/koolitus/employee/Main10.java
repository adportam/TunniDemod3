package ee.bcs.koolitus.employee;

import java.math.BigDecimal;

public class Main {

	public static void changeSalary(Employee employee) {
		/*employee = new Employee();
		employee.setSalary(BigDecimal.ZERO);
		employee.setSalary(employee.getSalary().add(BigDecimal.valueOf(100))); //NB! BIGDECIMALIS ei saa märke kasutada, vaid kasutatakse add, divide, substract jne...)
	miks ta consoolis väljutab 1000 ja 1010 ainult*/
	changeSalary(employee, BigDecimal.valueOf(100));
	
	}
	public static void changeSalary(Employee employee, BigDecimal salaryChange) {
		employee.setSalary(employee.getSalary().add(salaryChange));
	}
	
	public static void main(String[] args) {
		Employee employee = new Employee();
		employee.setSalary(BigDecimal.valueOf(1_000));
		changeSalary(employee);
		System.out.println(employee.getSalary());
		changeSalary(employee, BigDecimal.TEN);
		System.out.println(employee.getSalary());
	}
}
