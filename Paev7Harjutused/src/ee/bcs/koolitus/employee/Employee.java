package ee.bcs.koolitus.employee;

import java.math.BigDecimal;

public class Employee {
BigDecimal salary;
	
	public BigDecimal getSalary()  {
		return salary;
	}
	public void setSalary1(BigDecimal salary) {
		this.salary = salary;
	}
	public Employee setSalary(BigDecimal salary) {
		this.salary = salary;
		return this;
	}
}

