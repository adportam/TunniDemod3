package paev5kordusYlesanded;

public class EsimeneStringipakk {

	public static void main(String[] args) {
		String[][] countriesWithCapitalName = new String[5][4];
		String[] finland = { "Helsinki", "Helsingi", "Helsinki", "Soome" };
		String[] estonia = { "Tallinn", "Tallinn", "Tallinn", "Eesti" };
		String[] sweden = { "Stockholm", "Stockholm", "Stockholm", "Rootsi" };
		String[] denmark = { "Copenhagen", "Kopenhaagen", "Kobenhaven", "Taani" };
		String[] usa = { "Washington, D.C.", "Washington, D.C.", "Washington, D.C.", "USA" };
		countriesWithCapitalName[0] = finland;
		countriesWithCapitalName[1] = estonia;
		countriesWithCapitalName[2] = sweden;
		countriesWithCapitalName[3] = denmark;
		countriesWithCapitalName[4] = usa;

		System.out.println("---------------------");
		
		for (String[] countryWithCapitalNames : countriesWithCapitalName) {
			if (countryWithCapitalNames[0] != null) {
				System.out.println("Riik - " + countryWithCapitalNames[3] + ": pealinna nimi - " + countryWithCapitalNames[2]);
			}
		}
	}
}