package paev5kordusYlesanded;

public class D5Ylesanne1 {

	public static void main(String[] args) {
		String[][] countriesWithCapitalName = new String[5][4];
		String[] finland = { "Helsinki", "Helsingi", "Helsinki", "Soome" };
		String[] estonia = { "Tallinn", "Tallinn", "Tallinn", "Eesti" };
		String[] sweden = { "Stockholm", "Stockholm", "Stockholm", "Rootsi" };
		String[] denmark = { "Copenhagen", "Kopenhagen", "Kobenhaven", "Taani" };
		String[] usa = { "Washington, D.C.", "Washington, D.C.", "Washington, D.C.", "USA" };
		countriesWithCapitalName[0] = finland;
		countriesWithCapitalName[1] = estonia;
		countriesWithCapitalName[2] = sweden;
		countriesWithCapitalName[3] = denmark;
		countriesWithCapitalName[4] = usa;

		// trüki välja eestikeelsed nimed
		for (String[] countryWithCapitalNames : countriesWithCapitalName) {
			if (countryWithCapitalNames[0] != null) {
				System.out.println(countryWithCapitalNames[1]);
			}
		}
		System.out.println("---------------------");
		// Lause koostamine
		for (String[] countryWithCapitalNames : countriesWithCapitalName) {
			if (countryWithCapitalNames[0] != null) {
				System.out.println("Riik -" + countryWithCapitalNames[3] + ": pealinn - " + countryWithCapitalNames[2]);
			}
		}
		System.out.println("----------------");
		// ühel linnal võib olla kohalikes riigikeeltes keeltes mitu nimetust, variant 1
		// -
		// muudan linna mime tekstilist sisu nii, et linnade nimesid eraldab semikoolon

		System.out.println("---ver  1 -----------");
		countriesWithCapitalName[0][2] = "Helsinki;Hesingfors";
		// lause koostamine
		for (String[] countryWithCapitalNames : countriesWithCapitalName) {
			if (countryWithCapitalNames[0] != null) {
				System.out.println("Riik -" + countryWithCapitalNames[3] + ": pealinn - " + countryWithCapitalNames[2]);
				System.out.println("-------------------");
			}
		}
		System.out.println("---------------------");

		// ühel linnal võib olla kohalikes riigikeeltes keeltes mitu nimetust, variant 2
		// -
		// muudan massiivi, viin riigi nime esimesesks ning lisan massiivi lõppu
		// vajaliku arvu
		// veerge, esialgu 1
		System.out.println("---ver  2 -----------");

		countriesWithCapitalName[0][2] = "Helsinki";
		for (int i = 0; i < countriesWithCapitalName.length; i++) {
			// ajutine rea muutuja mida kasutan veergude ümbertõsteks, teenn selle kohe ühe
			// võrra
			// pikema, kui varasem oli
			String[] tempCountryRow = new String[countriesWithCapitalName[i].length + 1];
			// viimane veerg esimeseks
			tempCountryRow[0] = countriesWithCapitalName[i][3];
			// ülejäänud veerud nihkega 1 samm edasi
			for (int columnCount = 0; columnCount < countriesWithCapitalName[i].length - 1; columnCount++) {

				tempCountryRow[columnCount + 1] = countriesWithCapitalName[i][columnCount];
			}
			// vahetan rea tabeleis uue ajutise rea vastu
			countriesWithCapitalName[i] = tempCountryRow;
		}

	}
}
