package ee.bcs.koolitus.auto;

public class Main { //rekursiivne meetod on sisuline tsükkel iseeneses, mida kutsustakse meetodi sees...
	public static int factorialCalculation (int givenNumber) {
		int factorial = givenNumber;
		if (givenNumber <=1) {
			return factorial;
		} else {
			return factorialCalculation(givenNumber-1) * givenNumber;
		}
	}
	
public static String leiaSumma(String miskiTekst, int...arvud) { // ... on VAR ARGS ...tähendab et loob massiivi millega saab teha
		int summa = 0;
		for (int arv: arvud) {
		summa += arv;
		}
		return miskiTekst + summa;
	}
	public static void main(String[] args) {
		System.out.println(leiaSumma("Summa on: ", 1, 5, 8));
		System.out.println(leiaSumma("Nüüd on summa: ", 1, 5, 8, 10, 25));
		System.out.println(factorialCalculation(5));
		System.out.println("---------------------------");
		
		
		Mootor mootor1 = new Mootor ();
		mootor1.setVoimsus(77);
		mootor1.setKytuseTyyp(KytuseTyyp.PETROL);
		
		Mootor mootor2 = new Mootor ();
		mootor2.setVoimsus(66);
		mootor2.setKytuseTyyp(KytuseTyyp.DIESEL);
		
		Mootor mootor3 = new Mootor ();
		mootor3.setVoimsus(84);
		mootor3.setKytuseTyyp(KytuseTyyp.LPG);
		
		Mootor mootor4 = new Mootor ();
		mootor4.setVoimsus(80);
		mootor4.setKytuseTyyp(KytuseTyyp.CNG);
		
		Mootor mootor5 = new Mootor ();
		mootor5.setVoimsus(420);
		mootor5.setKytuseTyyp(KytuseTyyp.ELECTRIC);
		
		Mootor mootor6 = new Mootor ();
		mootor6.setVoimsus(600);
		mootor6.setKytuseTyyp(KytuseTyyp.HYDROGEN);
		
		System.out.println("Mootoril on võimsus " + mootor1.getVoimsus()
						+ "kW, kütusetüüp on " + mootor1.getKytuseTyyp());
		System.out.println("MOOTOR > " + mootor1); // jaava ise kirjutab toString meetodi,
		// seega ei pea koodilihtsustamiseks pikalt väljakirjutama
		System.out.println("---------------------");
		
		System.out.println("Mootoril on võimsus " + mootor2.getVoimsus()
		+ "kW, kütusetüüp on " + mootor2.getKytuseTyyp());
		System.out.println("MOOTOR > " + mootor2);
		System.out.println("---------------------");
		
		System.out.println("Mootoril on võimsus " + mootor3.getVoimsus()
		+ "kW, kütusetüüp on " + mootor3.getKytuseTyyp());
		System.out.println("MOOTOR > " + mootor3);
		System.out.println("---------------------");
		
		System.out.println("Mootoril on võimsus " + mootor4.getVoimsus()
		+ "kW, kütusetüüp on " + mootor4.getKytuseTyyp());
		System.out.println("MOOTOR > " + mootor4);
		System.out.println("---------------------");
		
		System.out.println("Mootoril on võimsus " + mootor5.getVoimsus()
		+ "kW, kütusetüüp on " + mootor5.getKytuseTyyp());
		System.out.println("MOOTOR > " + mootor5);
		System.out.println("---------------------");
		
		System.out.println("Mootoril on võimsus " + mootor6.getVoimsus()
		+ "kW, kütusetüüp on " + mootor6.getKytuseTyyp());
		System.out.println("MOOTOR > " + mootor6);
		System.out.println("---------------------");
		
	Auto autoSAAB = new Auto();
		autoSAAB.setKohtadeArv(5);
		autoSAAB.setMootor(mootor1);
		autoSAAB.setUsteArv(5);
		System.out.println(autoSAAB);
		System.out.println("SAAB  " + autoSAAB.getMootor().getKytuseTyyp());
		
	Auto autoVolvo = new Auto().setKohtadeArv(5).setMootor(mootor4).setUsteArv(5);
	/*moodus lühendatud versioonist, kus semikoolonid võetakse ära (setterid ei tohi olla VOIDid!!!!), seda saab teha ainult
	SETTERITEGA, mitte GETTERITEGA!!!! selline uus moodus tulid JAVA 8ga*/
		System.out.println(autoVolvo);
		System.out.println("VOLVO  " + autoVolvo.getMootor().getKytuseTyyp());
		
	Auto autoKOENIGSEGG= new Auto().setKohtadeArv(5).setMootor(mootor5).setUsteArv(2);
			System.out.println(autoKOENIGSEGG);
			System.out.println("KOENIGSEGG  " + autoKOENIGSEGG.getMootor().getKytuseTyyp());
			
	Auto autoZONDA= new Auto().setKohtadeArv(1).setMootor(mootor6).setUsteArv(2);
			System.out.println(autoZONDA);
			System.out.println("ZONDA  " + autoZONDA.getMootor().getKytuseTyyp());
	}
		
}
