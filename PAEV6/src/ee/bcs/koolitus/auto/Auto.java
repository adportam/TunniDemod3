package ee.bcs.koolitus.auto;

public class Auto {
	private int usteArv;
	private int kohtadeArv;
	private Mootor mootor;

	public Auto() { // loon tühja konsturktori
	}

	public Auto(int usteArv, Mootor mootor) {
		this.usteArv = usteArv;
		this.mootor = mootor;
	}

	public int getUsteArv() { // igal klassil on vaimiisi konstruktor, mis elab seni kui ma ise ei tee mingit
								// uut konstruktorit
		return usteArv;
	}

	public Auto setUsteArv(int usteArv) {
		this.usteArv = usteArv;
		return this;
	}

	public int arvutaKindlustus(String kasutajaNimi, int periood) {
		int summa = arvutaKindlustus(periood) + 10;
		if (kasutajaNimi.equals("Mari")) {
			summa += 15;
		} else if (kasutajaNimi.equals("Mati")) {
			summa += 45;
		}
		return summa;
	}

	public int arvutaKindlustus(int vanus, int periood) {
		return (vanus < 26) ? arvutaKindlustus(periood) * 2 : arvutaKindlustus(periood);
	}

	public int arvutaKindlustus(int periood) {
		return periood * 10;
	}

	public int getKohtadeArv() {
		return kohtadeArv;
	}

	public Auto setKohtadeArv(int kohtadeArv) {
		this.kohtadeArv = kohtadeArv;
		return this;
	}

	public Mootor getMootor() {
		return mootor;

	}

	public Auto setMootor(Mootor mootor) {
		this.mootor = mootor;
		return this;
	}

	@Override
	public String toString() {
		return "Auto[ustearv=" + usteArv + ", kohtadeArv=" + kohtadeArv + ", mootor = " + mootor + "]";

	}
}
