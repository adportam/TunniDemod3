package ee.bcs.koolitus.auto;

public enum KytuseTyyp {
	DIESEL, PETROL, LPG, CNG, ELECTRIC, HYDROGEN;
}
