
public class Day4DoWhile {

	public static void main(String[] args) {
		int i =8;
		while (i<8) {
			System.out.println("while ütleb et i = " + i);
			i++;
		}
		int j =8;
		do {
			System.out.println("Do-while ütleb, et j =" + j);
			j++;
		}while (j<8);

	}

}

// näitab while ja whileDo erinevust 2 puhul on oluline see, et teeb ikka vähemalt korra ja siis ütleb, esimese puihul saab areu , et mitte ja läheb do poole edasi