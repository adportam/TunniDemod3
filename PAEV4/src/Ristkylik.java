
public class Ristkylik {

	public static void main(String[] args) {
		int [][] ristkylik = { {1, 0},
								{2,5 },
								{98, 75} };
		
		for (int rowNr=0; rowNr < ristkylik.length; rowNr++) {
			int firstSide = 0;
			int secondSide = 0;
			for (int columnNr=0; columnNr < ristkylik[rowNr].length; columnNr++) {
				if(rowNr == 0) {
					firstSide = ristkylik[rowNr][columnNr];
				} else {
					secondSide = ristkylik[rowNr][columnNr];
				}
			}
			System.out.println(("Ristkülikul külgedega ") + firstSide + "and" + secondSide + (firstSide * secondSide));
		}
	}
}

