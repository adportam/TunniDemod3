import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day4RistkylikuListArvutused {

	public static void main(String[] args) {
		List<List<Double>> ristkylikuMoodud = new ArrayList<>();
		ristkylikuMoodud.addAll(Arrays.asList(Arrays.asList(3.5 , 8.0),
												Arrays.asList(0.5, 2.0),
												Arrays.asList(50.0, 8.5)));
		int summa = 0;
		for (List<Double> yheRistkylikuMoodud : ristkylikuMoodud) {
			summa+=yheRistkylikuMoodud.get(0) * yheRistkylikuMoodud.get(1);
			System.out.println("Ristküliku pindala on " + (yheRistkylikuMoodud.get(0) * yheRistkylikuMoodud.get(1)));
			System.out.println("Ristkülikute pindalade summa on " + summa + " (Ü2)");
			System.out.println("----------------------------------------------------------------");
			
			
			double summa2 = 0;
			for (int i = 0;i< ristkylikuMoodud.size(); i++) {
				summa2+=ristkylikuMoodud.get(i).get(0) * ristkylikuMoodud.get(i).get(1);
				System.out.println("Ristküliku pindala on " + (ristkylikuMoodud.get(i).get(0)*ristkylikuMoodud.get(i).get(1)));
			}
		}
	}
}


