import java.util.Set;
import java.util.TreeSet;

public class DAY4TreeSET {

		public static void main(String[] args) {
			Set<String> nimed = new TreeSet<>(); // alati tähestiku jätrjekorras setis paigaldatuna
			nimed.add("Mati");
			nimed.add("Kati");
			nimed.add("Mari");
			System.out.println(nimed);
			nimed.add("Mati");
			nimed.add("Jüri");
			System.out.println(nimed);
			for(String nimi: nimed) {
			System.out.println(nimi.hashCode());
			}
		nimed.remove("Mati");
		System.out.println(nimed);
		System.out.println("kas nimekirjas on Muri? - " + nimed.contains("Muri"));
		System.out.println("kas nimekirjas on Abdullah? - " + nimed.contains("Abdullah"));
		System.out.println("kas nimekirjas on Mahmu? - " + nimed.contains("Mahmu"));
		System.out.println("Setis on " + nimed.size() + "elementi");
		}
	}
