import java.util.HashMap;
import java.util.Map;

public class Day4Maps {
															
	public static void main(String[] args) {
		// string  on Key
		// String 2 on väärtuse tüüp
		//treemap reastab key võtmete järgi
		Map<String, String> inimesed = new HashMap<>();
		inimesed.put("12345678912", "Mati Karu");
		inimesed.put("12345678913", "Kati Maru");
		inimesed.put("12345678928", "Mummi Taru");
		System.out.println(inimesed);
		
		System.out.println("kellel on inikukood 12345678928?" + inimesed.get("12345678928"));
		
		for (String key : inimesed.keySet()) {
			if (inimesed.get(key).equals("Mummi Taru")) {
				System.out.println("Mummo Taru inikukood on " + key);
			}
		}
		
		System.out.println("Kas Mummi Taru nimeline on loetelus? " + inimesed.containsValue("Mummi Taru"));
		

	}

}
