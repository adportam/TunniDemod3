package day2_DataTypes;

public class DataTypeInstruction {

	public static void main(String[] args) {
		byte esimeneByte = 5;
		byte teineByte = 5;
		byte kolmasByte = 55;
		byte neljasByte = 9;
		byte viiesByte = 99;
		byte kuuesByte = 6;
				
		Integer test =15;
		double esimeneDouble =5;
		
		System.out.println("esimene byte = " + esimeneByte);
		System.out.println("teine byte = " + teineByte * kolmasByte / neljasByte + test);
		System.out.println("kolmas byte = " + kolmasByte);
		System.out.println("neljas byte = " + neljasByte);
		System.out.println("viies byte = " + viiesByte % neljasByte);
		System.out.println("kuues byte = " + (esimeneDouble));
		
		char esimeneChar = 'a';
		char teineChar = 'h';
		System.out.println(esimeneChar);
		System.out.println(teineChar);
		System.out.println("" + esimeneChar + teineChar);
		System.out.println(esimeneChar == teineChar);
		char kolmasChar = 'a';
		System.out.println(esimeneChar == kolmasChar);
		
		int counter = 5;
		counter = counter +1;
		System.out.println("counter =" + counter);
		counter++;
		System.out.println("counter =" + (counter + 9));
		
		/* counter ++ ja ++ counter*/
		
		int a = 5;
		int b = ++a;
		System.out.println("a = " + a + "; b = " + b);
		b = a++;
		System.out.println("a = " + a + "; b = " + b);
		
		/* jälgi a väärtuse lineaarsust koodireas, järgmisega muudad lineaarsusel a väärtust näöitkes 100ga */
		
		a = 100;
		a +=5;
		System.out.println("a = " + a);
		a = a + 5;
		System.out.println("a = " + a);
		
		System.out.println(a%2);
		System.out.println(a%10);
		System.out.println(a%3);
		System.out.println();
		
		String esimeneLause = "Jõmmu ei oska puu otsast alla tula";
		System.out.println(esimeneLause);
		
		String eesnimi ="Henn";
		String perenimi ="Sarv";
		
		String koguNimi = eesnimi + " " + perenimi;
		System.out.println(koguNimi);
		System.out.println("Henn Sarv" == koguNimi);
		System.out.println("Henn Sarv" .equals(koguNimi));
		
		String koheKoguNimi = "Henn Sarv";
		
		System.out.println(koheKoguNimi == koguNimi);
		System.out.println(koheKoguNimi.equals(koguNimi));
		
		String kymme ="10";
		int arvKymme = Integer.parseInt(kymme);
		System.out.println(arvKymme);
		
		String kymme2 ="20";
		double doubleArv = Double.parseDouble(kymme2);
		System.out.println(doubleArv);
		String textDoublest = ((Double) doubleArv). toString();
		textDoublest = "" + doubleArv;
			
		// viimast kaks rida on samad, kuid viimane on huvitav lahend, eelviimasel kasutuses kaasting//
		}
}
		
	
