package paev8Abstract;

public class Main {
	
	public static void main(String[] args) { //Õpi PÄHE, raisk!
		
		Inimene inimene1 = new Opilane();
		
		inimene1.setNimi("Mati");
		inimene1.soomine();
			 
		Opilane opilane = new Opilane();
		opilane.setNimi("Kati");
		opilane.soomine();
		Inimene opetaja = new Opetaja(); // kasutakse esimest ja viimast
		Tootaja opetaja2 = new Opetaja();// kasutatakse suht harva
		opetaja2.setNimi("Õpetaja PELLE");
		opetaja2.soomine();
		Opetaja opetaja3 = new Opetaja();// kasutakase seda ja esimest
		opetaja3.setNimi("Õpetaja LAUR");
		Koristaja koristaja = new Koristaja();
		koristaja.setNimi("Tädi MAIMU");
		koristaja.soomine();
		
	}
}
