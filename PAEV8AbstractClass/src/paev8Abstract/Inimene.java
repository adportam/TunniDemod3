package paev8Abstract;

public abstract class Inimene {
	private String nimi;

	abstract void soomine();

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

}
