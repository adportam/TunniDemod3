package test;

import paev8Interface.Kujund;
import paev8Interface.Ruut;

public class RuutTest {
	
	@Test
	public void testPindalaArvutamine( ) {
		Kujund ruut = new Ruut().setKyljePikkus(4);
		double pindala = ruut.arvutaPindala();
		Assert.assertEquals(2.5, pindala, 0.0);
	}

}
