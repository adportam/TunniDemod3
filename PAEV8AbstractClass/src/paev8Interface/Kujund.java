package paev8Interface;

public interface Kujund {
	double arvutaPindala();
	// kõik on abstrakt interfaces seega saab kirjutada
	// kompilaator saab sellest aru ning pole vaja " public abstract" välja kirjutada
}
