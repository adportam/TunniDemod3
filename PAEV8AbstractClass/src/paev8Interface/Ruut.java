package paev8Interface;

public class Ruut implements Kujund {
	private double kyljePikkus;

	@Override
	public double arvutaPindala() {
		// return kyljePikkus*kyljePikkus;
		return Math.pow(kyljePikkus, 2);

	}

	public double getKyljePikkus() {
		return kyljePikkus;
	}

	public Ruut setKyljePikkus(double kyljePikkus) {
		this.kyljePikkus = kyljePikkus;
		return this;
	}
}
