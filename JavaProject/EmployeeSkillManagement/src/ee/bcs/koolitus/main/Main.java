package ee.bcs.koolitus.main;

import java.util.Set;

import ee.bcs.koolitus.employeeskillmanagement.dao.Skills;
import ee.bcs.koolitus.employeeskillmanagement.resource.SkillsResource;



public class Main {
	public static void main(String[] args) {
		Set<Skills> skill;
		SkillsResource resource = new SkillsResource();
		skill = resource.getAllSkills();

		System.out.println(skill);
	}
}
