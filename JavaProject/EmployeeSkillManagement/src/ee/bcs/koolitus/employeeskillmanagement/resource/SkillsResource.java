package ee.bcs.koolitus.employeeskillmanagement.resource;

import java.util.Set;
import java.util.HashSet;
import java.sql.ResultSet;
import java.sql.SQLException;

import ee.bcs.koolitus.employeeskillmanagement.dao.Skills;

public class SkillsResource {


	public Set<Skills> getAllSkills() { 
		Set<Skills> skill = new HashSet<>();
		String sqlQuery = "SELECT * FROM skills";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Skills skills = new Skills().setId(results.getInt("id"))
						.setSkill(results.getString("skill"))
						.setSkillGroupId(results.getInt("skill_group_id"));
				
				skill.add(skills);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address set:" + e.getMessage());
		}

		return skill;
	}
			
}

	
