package ee.bcs.koolitus.employeeskillmanagement.dao;

public class Skills {
	private int id;
	private String skill;
	private int skillGroupId;
	
	public int getId() {
		return id;
	}
	public Skills setId(int id) {
		this.id = id;
		return this;
	}
	public String getSkill() {
		return skill;
	}
	public Skills setSkill(String skill) {
		this.skill = skill;
		return this;
	}
	public int getSkillGroupId() {
		return skillGroupId;
	}
	public Skills setSkillGroupId(int skillGroupId) {
		this.skillGroupId = skillGroupId;
		return this;
	}
	
	@Override
	public String toString() {
		return "Skills[id=" + id + ", skill=" + skill + ", skillGroupId=" + skillGroupId + "]";
	}

}
