package emloyeeDay10;

import java.math.BigDecimal;
import java.util.Date;

public class Employee10 {
	private String firstName;
	private String middleName;
	private String lastName;
	private Date birthDate;
	private Date slaveDate;
	private BigDecimal salary;
		
	public Employee10() {
	}
	
		public Employee10(String firstName, String middleName, String lastName, Date birthDate, Date slaveDate,
			BigDecimal salary) {
		super();
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.slaveDate = slaveDate;
		this.salary = salary;
	}
		public BigDecimal getSalary()  {
			return salary;
		}
		public Employee10 setSalary1(BigDecimal salary) { //setterid vajavad VOID muutumist Employee10ks, getterid mitte
			this.salary = salary;
			return this;
		}
		public Employee10 setSalary(BigDecimal salary) {
			this.salary = salary;
			return this;
		}
		public String getFirstName() {
			return firstName;
			
		}
		public Employee10 setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}
		public String getMiddleName() {
			return middleName;
		}
		public Employee10 setMiddleName(String middleName) {
			this.middleName = middleName;
			return this;
		}
		public String getLastName() {
			return lastName;
		}
		public Employee10 setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}
		public Date getBirthDate() {
			return birthDate;
		}
		public Employee10 setBirthDate(Date birthDate) {
			this.birthDate = birthDate;
			return this;
					}
		public Date getSlaveDate() {
			return slaveDate;
		}
		public Employee10 setSlaveDate(Date slaveDate) {
			this.slaveDate = slaveDate;
			return this;
		}
		//inner builder NB! innerclassi peab muutuja muutumisel Employee10s peab builder ka seda nägema, kui on kustutamine, siis on viga väljundis
		public static class Employee10Builder { // static on sellepärast, et ta ei teeks lisabuilderedi protsessis
			private String firstName;
			private String middleName;
			private String lastName;
			private Date birthDate;
			private Date slaveDate;
			private BigDecimal salary;
			
			public Employee10Builder firstName(final String firstName) {
				this.firstName = firstName;
				return this;
			}
				
			public Employee10Builder middleName(final String middleName) {
				this.middleName = middleName;
				return this;
			}
			public Employee10Builder lastName(final String lastName) {
					this.lastName = lastName;
					return this;
			}
			public Employee10Builder birthDate(final Date birthDate) {
				this.birthDate = birthDate;
				return this;
			}
			public Employee10Builder slaveDate(final Date slaveDate) {
				this.slaveDate = slaveDate;
				return this;
			}
			public Employee10 build() {
				return new Employee10 (firstName, middleName, lastName, birthDate, slaveDate, salary);
			}
		}
	}

