package emloyeeDay10;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import emloyeeDay10.Employee10.Employee10Builder;

public class Main10 {

	public static void changeSalary(Employee10 employee10, BigDecimal salaryChange) {
		employee10.setSalary(employee10.getSalary().add(salaryChange));
	}

	public static void main(String[] args) throws ParseException {
		// Employee10 employee10 = new Employee10();
		// employee10.setSalary(BigDecimal.valueOf(1_000));
		// changeSalary(employee10);
		// System.out.println(employee10.getSalary());
		// changeSalary(employee10, BigDecimal.TEN);
		// System.out.println(employee10.getSalary());

		Employee10 employeeWithBigConstructor = new Employee10("Mari", "", "Kask",
				new SimpleDateFormat("dd-MM-yyyy").parse("21-08-1992"), // ära usnusta parse teeb stringist date vastupidi format date stringiks																	// date
				new SimpleDateFormat("dd-MM-yyyy").parse("02-02-2015"), null);

		Employee10 employeeWithBuilder = new Employee10.Employee10Builder().firstName("Mati").middleName("").lastName("Tamm")
				.birthDate(new SimpleDateFormat("dd-MM-yyyy").parse("21-11-1974"))
				.slaveDate(new SimpleDateFormat("dd-MM-yyyy").parse("18-10-2007")).build();

		System.out.println(new SimpleDateFormat("dd-MM-yyyy").format(employeeWithBuilder.getBirthDate()));
		System.out.println(new SimpleDateFormat("dd-MM-yyyy").format(employeeWithBuilder.getSlaveDate()));
		//System.out.println(new SimpleDateFormat("dd-MM-yyyy").format(employeeWithBuilder.getMiddleName())); // viga, sest tgemist on DATE mitte stringiga hahhaaaa
	}

}