package DAY12EmployeeManagement.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import DAY12EmployeeManagement.dao.Address;

public class AddressResource { // võib kasutada map, set , list
	public static Set<Address> getAllAddresses() { // muudetud abstraktseks läbi static lisamise, ehk ei saa uusi
													// objekte luua
		Set<Address> addresses = new HashSet<>();
		String sqlQuery = "SELECT * FROM address";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Address address = new Address().setId(results.getInt("id")).setCounty(results.getString("county"))
						.setCity(results.getString("city")).setVillage(results.getString("village"))
						.setHouse_no(results.getString("House_no")).setFlat_no(results.getString("flat_no"))
						.setFarm_name(results.getString("farm_name"));

				addresses.add(address);

			}
		} catch (SQLException e) {
			System.out.println(" Error on getting address set: " + e.getMessage());
		}
		return addresses;
	}

	public static Address getAddressById(int address_id) { // lisatud static
		Address address = null;
		String sqlQuery = "SELECT * FROM address WHERE id=" + address_id;
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				address = new Address().setId(results.getInt("id")).setCounty(results.getString("county"))
						.setCity(results.getString("city")).setVillage(results.getString("village"))
						.setHouse_no(results.getString("House_no")).setFlat_no(results.getString("flat_no"))
						.setFarm_name(results.getString("farm_name"));

			}
		} catch (SQLException e) {
			System.out.println(" Error on getting address set: " + e.getMessage());

		}
		return address;
	}

	public static void updateAddress(Address address) {
		String sqlQuery = "UPDATE address SET county='" + address.getCounty() + "', city='" + address.getCity()
				+ "', village='" + address.getVillage() + "', street='" + address.getStreet() + "', house_no='"
				+ address.getHouse_no() + "', flat_no='" + address.getFlat_no() + "', farm_name='"
				+ address.getFarm_name() + "' WHERE id =" + address.getId(); // ID juures ei pea üksik ülakoma olema
		System.out.println(sqlQuery);

		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on updating address");
			}
		} catch (SQLException e) {

			System.out.println("Error on getting address set:" + e.getMessage());
		}
	}

	public static void deleteAddress(Address address) { // lisatud hiljem static
		String sqlQuery = "DELETE from address WHERE id=" + address.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on deleting address");
			}
		} catch (SQLException e) {

			System.out.println("Error on getting address set:" + e.getMessage());
		}
	}

	public static Address addAddress(Address address) { // lisatud hiljem static
		String sqlQuery = "INSERT INTO address(county, city, village, street, house_no, flat_no, Farm_name)"
				// kuna üleval on märgitud, et võib olla ka null
				// (Address address = null;), siis võib täita ainult paar komplekti, kuid hiljem
				// tekib probleeme...ja kala kus peab märkima ja jebima lisaks.. hahahaaaaa
				+ "VALUES ('" + address.getCounty() + "', '" + address.getCity() + "', '" + address.getVillage()
				+ "', '" + address.getStreet() + "', '" + address.getHouse_no() + "', '" + address.getFlat_no() + "', '"
				+ address.getFarm_name() + "')";
		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				address.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println("Error on adding address: " + e.getMessage());
		}

		return address;
	}

}
