package DAY12EmployeeManagement.dao;

public class Address {
	private int id;
	private String county;
	private String city;
	private String village;
	private String street;
	private String house_no;
	private String flat_no;
	private String farm_name;
	
	
public int getId() {
		return id;
	}


	public Address setId(int id) {
		this.id = id;
		return this;
	}


	public String getCounty() {
		return county;
	}


	public Address setCounty(String county) {
		this.county = county;
		return this;
	}


	public String getCity() {
		return city;
	}


	public Address setCity(String city) {
		this.city = city;
		return this;
	}

	public String getVillage() {
		return village;
	}


	public Address setVillage(String village) {
		this.village = village;
		return this;
	}


	public String getStreet() {
		return street;
	}


	public Address setStreet(String street) {
		this.street = street;
		return this;
	}


	public String getHouse_no() {
		return house_no;
	}


	public Address setHouse_no(String house_no) {
		this.house_no = house_no;
		return this;
	}


	public String getFlat_no() {
		return flat_no;
	}


	public Address setFlat_no(String flat_no) {
		this.flat_no = flat_no;
		return this;
	}


	public String getFarm_name() {
		return farm_name;
	}


	public Address setFarm_name(String farm_name) {
		this.farm_name = farm_name;
		return this;
	}


@Override
public String toString() {
		return "Address[id" + id + "county=" + county + ", city=" + city + ", village=" + village + ", street=" + street + ", house_no=" + house_no + ", flat_no=" + flat_no +", farm_name=" + farm_name + "]";
	}
}