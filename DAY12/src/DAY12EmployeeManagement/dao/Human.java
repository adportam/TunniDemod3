package DAY12EmployeeManagement.dao;

import java.util.Date;

public class Human {
	private int id;
	private String personal_id_code;
	private String firstname;
	private String lastname;
	private Date birthday;
	private char gender;
	private int address_id;

	public int getId() {
		return id;
	}

	public Human setId(int id) {
		this.id = id;
		return this;
	}

	public String getPersonal_id_code() {
		return personal_id_code;

	}

	public Human setPersonal_id_code(String personal_id_code) {
		this.personal_id_code = personal_id_code;
		return this;

	}

	public String getFirstname() {
		return firstname;
	}

	public Human setFirstname(String firstname) {
		this.firstname = firstname;
		return this;
	}

	public String getLastname() {
		return lastname;
	}

	public Human setLastname(String lastname) {
		this.lastname = lastname;
		return this;
	}

	public Date getBirthday() {
		return birthday;
	}

	public Human setBirthday(Date birthday) {
		this.birthday = birthday;
		return this;
	}

	public char getGender() {
		return gender;
	}

	public Human setGender(char gender) {
		this.gender = gender;
		return this;
	}

	public int getAddress_id() {
		return address_id;
	}

	public Human setAddress_id(int address_id) {
		this.address_id = address_id;
		return this;
	}

	@Override
	public String toString() {
		return "Human[id=" + id + ", personal_id_code=" + personal_id_code + ", firstlname=" + firstname + ", lastname=" + lastname + ", birthday=" + birthday + ", gender=" + gender + ", address_id=" + address_id + "]";
		
	}
}
