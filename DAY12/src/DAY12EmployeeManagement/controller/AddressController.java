package DAY12EmployeeManagement.controller;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import DAY12EmployeeManagement.dao.Address;
import DAY12EmployeeManagement.resource.AddressResource;

@Path("/addresses") // läheb URLi pathi ja läheb @gethi
public class AddressController {

	@GET
	@Produces(MediaType.APPLICATION_JSON) // meetodiga väljastatkse applicatsioon JSON
	public Set<Address> getAllAddresses() {
		return AddressResource.getAllAddresses(); // AddressResource ja getAddresses muudetud (AddressRessource kaustas)
													// staatiliseks

	}

	@GET
	@Path("/{addressidId}") // täiendan pathi, läheb number, ehk id, seleks et välja kõüsida kasutan muutuja
							// (PathParam)
	@Produces(MediaType.APPLICATION_JSON) // meetodiga väljastatkse applicjson ja NB! ühe näiteks id parameetri kaudu
	public Address getAddressesbyAddressId(@PathParam("addressidId") int addressId) {
		// NB! int addressId on int seepärast, et postmanis saab küsida lisades numbrit,
		// ehk brouseri URL on string,
		// kui määrad int value numbriga, siis ta saab aru, et üks meetodi väljakutsuja
		// on number, seega postmanis GET käsk /7 toob esile Id 7
		// lisamisega pathi @Path ("/{addressidId}") muutuja peab olema sama kui
		// PathPara "addressidId"
		return AddressResource.getAddressById(addressId);
	}

	@POST // andmete lisamine
	@Produces(MediaType.APPLICATION_JSON) // loobjsoni
	@Consumes(MediaType.APPLICATION_JSON) // vajab sisendiks jsonit
	public Address addNewAddress(Address address) {
		return AddressResource.addAddress(address);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON) // loob jsoni
	@Consumes(MediaType.APPLICATION_JSON) // vajab sisendiks jsonit
	public void updateAddresses(Address address) {
		AddressResource.updateAddress(address);
	}
	/*
	 * @DELETE
	 * 
	 * @Path("/{addressId}") public void deleteAddress(@PathParam("addressId") int
	 * addressId) { AddressResource.deleteAddressByAddressId(addressId); }
	 */

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteAddress(Address address) {
		AddressResource.deleteAddress(address);

	}
}
