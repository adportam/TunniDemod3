package DAY12EmployeeManagement.main;

import java.util.Set;

import DAY12EmployeeManagement.dao.Address;
import DAY12EmployeeManagement.resource.AddressResource;

public class Main {

	public static void main(String[] args) {
		Set<Address> addresses;
		AddressResource resource = new AddressResource();
		addresses = resource.getAllAddresses();
		System.out.println(addresses);
		
		// Address addressId3 = resource.getAddressById(3);
		
		/*addressId3.setFarm_name("Kassi talu").setVillage("Adler"); //, ehk tegemist on andmete muutmisega
		resource.updateAddressById(addressId3);
		System.out.println(addressId3);
		*/
		
		// resource.deleteAddress(addressId3); //NB! delete on lõplik, kuid on võimalik ka arhiveerida (aktiine, mitteaktiivne, true-false väärtused... jms abra-kadabra)
		
		// kui tahad nr1 deletida, siis
		// Address addressId1 = resource.getAddressById(1);
		// addressId1 = resource.getAddressById(1);
		// System.out.println(addressId1);
		
		// addressId3 = resource.getAddressById(3);
		// System.out.println(addressId3);
		/*
		Address addressNew = new Address().setCity(("Võru")).setStreet("Pikk");
		resource.addAddress(addressNew);
		System.out.println(addressNew);
//		*/
	}
}
