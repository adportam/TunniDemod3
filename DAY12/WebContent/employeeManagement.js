alert("Tervist mu Daamid ja Härrad");
var employee = "Mari";

console.log("töötaja on " + employee);
employee = {
	"nimi" : "mari",
	"id" : 1
};
console.log(employee);
console.log("Jsonis on töötaja nimi; " + employee.nimi);

function arvutaSumma(a, b) {
	return a + b;

}
console.log(arvutaSumma(2, 3));

function loenda(loendaMitmeni) {
	for (var i = 1; i <= loendaMitmeni; i++) {
		console.log("nüüd on arv " + i);

	}
	// function loenda(loendaMitmeni) {
	for (var i = 1; i >= loendaMitmeni; i++) {
		console.log("nüüd on arv " + i);
	}
}

loenda(7);

// javascriptiga JSONi kirjutamine brrrrrrrrrr brauseri F12 consooli
// ehk JASON objekti loomine

var employees = {
	"employees" : [ {
		"employee" : {
			"eesnimi" : "mari",
			"perekonnanimi" : "Maasikas",
			"id" : 1
		}
	}, {
		"employee" : {
			"eesnimi" : "mati",
			"perekonnanimi" : "Tamm",
			"id" : 3
		}
	}, {
		"employee" : {
			"eesnimi" : "risto",
			"perekonnanimi" : "Kivi",
			"id" : 9
		}
	} ]
};

function fillTableBody() {
	var tableBody = document.getElementById("generatedEmployeesTableBody");
	var tableBodyContent = ""
	for (var i = 0; i < employees.employees.length; i++) {
		tableBodyContent = tableBodyContent + "<tr><td>"
				+ employees.employees[i].employee.id + "</td><td>"
				+ employees.employees[i].employee.eesnimi + "</td><td>"
				+ employees.employees[i].employee.perekonnanimi + "</td><td>"
	}
	tableBody.innerHTML = tableBodyContent;

}
fillTableBody();

function printEmployees() {
	for (var i = 0; i < employees.employees.length; i++) {
		console.log(employees.employees[i].employee.nimi);
		// (employees //esimene on muutuja nimi VARi järgi
		// .employees - tuleb JSONist
		// [i].employee
		// .nimi);

	}
}
printEmployees();

var allAddresses;
function getAllAddresses() {

	$
			.getJSON(
					"http://localhost:8080/EmployeeManagement/rest/addresses",
					function(addresses) {
						console.log(addresses);
						allAddresses = addresses;
						console.log(addresses);
						var tableBody = document
								.getElementById("addressTableBody");
						var tableContent = "";
						for (var i = 0; i < addresses.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									+ addresses[i].id
									+ "</td><td>"
									+ checkIfNullOrUndefined(addresses[i].county)
									+ "</td><td>"
									+ checkIfNullOrUndefined(addresses[i].city)
									+ "</td><td>"
									+ checkIfNullOrUndefined(addresses[i].village)
									+ "</td><td>"
									+ checkIfNullOrUndefined(addresses[i].street)
									+ "</td><td>"
									+ checkIfNullOrUndefined(addresses[i].houseNo)
									+ "</td><td>"
									+ checkIfNullOrUndefined(addresses[i].flatNo)
									+ "</td><td>"
									+ checkIfNullOrUndefined(addresses[i].farmName)
									+ "</td><td><button type='button' onClick=' deleteAddress("
									+ addresses[i].id
									+ ")'>DELETE</button>"
									+ "<button type='button' onClick=' fillModifyForm("
									+ addresses[i].id + ")'>MODIFY</button>"
									+ "</td></tr>";

						}
						tableBody.innerHTML = tableContent;

					});
}
getAllAddresses();

function checkIfNullOrUndefined(value) {
	if (value != null && value != undefined && value != "null") {
		return value;
	} else {
		return "";
	}
}

function addNewAddress() {
	var newAddressJson = {
		"county" : document.getElementById("county").value,
		"city" : document.getElementById("city").value,
		"street" : document.getElementById("street").value
	}
	console.log(newAddressJson);
	var newAddress = JSON.stringify(newAddressJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeManagement/rest/addresses",
		type : "POST",// päringu tüüp
		data : newAddress,
		contentType : "application/json; charset=utf-8", // defineerime
															// päringu sisu, ehk
															// teeme JSONit
		success : function() {// kui tegevus on õnnelik siis mis toimub
			alert("Well done, my Friend");
			getAllAddresses();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}

	});

}

function deleteAddress(addressId) {
	var deleteAddressJson = {
		"id" : addressId
	}
	$.ajax({
		url : "http://localhost:8080/EmployeeManagement/rest/addresses",
		type : "DELETE",// päringu tüüp
		data : JSON.stringify(deleteAddressJson),
		contentType : "application/json; charset=utf-8", // defineerime
															// päringu sisu, ehk
															// teeme JSONit
		success : function() {// kui tegevus on õnnelik siis mis toimub
			alert("DELETE executed!!! Well done, my Friend");
			getAllAddresses();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}

	});
}

function fillModifyForm(addressId) {
	for (var i = 0; i < allAddresses.length; i++) {
		if (allAddresses[i].id == addressId) {
			document.getElementById("idModify").value = addressId;
			document.getElementById("countyModify").value = allAddresses[i].county;
			document.getElementById("cityModify").value = allAddresses[i].city;
			document.getElementById("WillageModify").value = allAddresses[i].Willage;
			document.getElementById("HouseNoModify").value = allAddresses[i].HouseNo;
			document.getElementById("FlatNoModify").value = allAddresses[i].FlatNo;
			document.getElementById("FarmModify").value = allAddresses[i].Farm;
		}
	}
}

function changeAddress() {
	var modifyAddressJson = {
		"id" : document.getElementById("idModify").value,
		"county" : document.getElementById("countyModify").value,
		"city" : document.getElementById("cityModify").value,
		"Willage" : document.getElementById("WillagetModify").value,
		"street" : document.getElementById("streetModify").value,
		"HouseNo" : document.getElementById("HouseNoModify").value,
		"FlatNo" : document.getElementById("FlatNoModify").value,
		"Farm" : document.getElementById("FarmModify").value
		
	}
	console.log(modifyAddressJson);
	var modifyAddress = JSON.stringify(modifyAddressJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeManagement/rest/addresses",
		type : "PUT",// päringu tüüp
		data : modifyAddress,
		contentType : "application/json; charset=utf-8", // defineerime
															// päringu sisu, ehk
															// teeme JSONit
		success : function() {// kui tegevus on õnnelik siis mis toimub
			alert("Value changed! Well done, my Friend");
			getAllAddresses();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});

}