package paev9;

import java.awt.Color;
import java.util.Objects;

public abstract class dasAuto implements Comparable <dasAuto> {

	private String regNr;
	private Color color; // võib ka string, sellel juhul ei pea color'it importima, kirjutad ise ("yellow");
	private String vinCode;
	private int doorsNr;
	
	public dasAuto() {
	}
	public dasAuto(String regNr, Color color, String vinCode, int doorsNr) {
	this.regNr = regNr;
	this.color = color;
	this. vinCode = vinCode;
	this.doorsNr = doorsNr;
	}
	/**
	@Override //loogiline } sulg pab alati ees olema
	public boolean equals(Object obj) { // alati loogilise sulu algus {
		if(obj == null) { // alati loogilise sulu algus {
		return false; 
		}
		if(this == obj) {
			return true;
		}
		if(getClass() !=obj.getClass()) {
			return false;
		}
		final dasAuto other = (dasAuto) obj; // () castimine, sest ilma selleta ei saa küsida objekti kohta muutujaid
		boolean variablesEqual = this.vinCode.equals(other.vinCode); // kui vin on unikaalne, siis ei pea teisi väljakirjutada
		// 
		return variablesEqual;
		
	}
	
	/**@Override
	public int hashCode() {
		int hash =5;
		hash = 39 * hash +Objects.hashCode(regNr); // lühem koodiväljendus: hash = 39 * hash + regNr.hashCode();
		hash = 39 * hash +Objects.hashCode(color);// lühem koodiväljendus: hash = 39 * hash + color.hashCode();
		hash = 39 * hash +Objects.hashCode(vinCode);// lühem koodiväljendus: hash = 39 * hash + vinCode.hashCode();
		hash = 39 * hash +Objects.hashCode(doorsNr);// hash = 39 * hash + vinCode.hashCode; NB!!! (); puudub kuna tegemist on integeriga,
													//ehk luhtsalt number, mis ei kutsu välja meetodit, seega number algselt ju fikseeitud by objekti,
													//või juba dasAuto default
		// lühem koodiväljendus: hash = 39 * hash + Integer.valueOf(doorsNr).hashCode();
		return hash;
		
		/**@Override }
		return ();
			*/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + doorsNr;
		result = prime * result + ((regNr == null) ? 0 : regNr.hashCode());
		result = prime * result + ((vinCode == null) ? 0 : vinCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		dasAuto other = (dasAuto) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (doorsNr != other.doorsNr)
			return false;
		if (regNr == null) {
			if (other.regNr != null)
				return false;
		} else if (!regNr.equals(other.regNr))
			return false;
		if (vinCode == null) {
			if (other.vinCode != null)
				return false;
		} else if (!vinCode.equals(other.vinCode))
			return false;
		return true;
		
		@Override
		public String toString() {
			return "dasAuto [vinCode=" + vinCode + regNr=", + regNr +  ", color=" + color + " + doorsNr + "]";
			
		@Override
		public int compareTo(dasAuto otherdasAuto);
		if(this.equals(otherDasAuto)) {
			return 0;
			
		}
		
		}
	}
	private String regNr(String string) {
		// TODO Auto-generated method stub
		return null;
	}
}


