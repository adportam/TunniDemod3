package EXCEPTIONS;

public class Human {
	
	private String name;
	
	public String getName() {
		return name;
	}
	public Human setName(String name) throws NameTooShortException {
			if (name.length() <2 ) {
				throw new NameTooShortException("Humans need two characters to name itself");
				
			}
			this.name = name;
			return this;
	}
}
