package EXCEPTIONS;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class exceptionsHandling {

	public static void main(String[] args) {
		/**
		try (FileWriter writer = new FileWriter(new File("outputResults.txt"));)
			
			ehk, try järgi sulus moodustada ja pann kogu FileWriter sulgudesse koos semikoololiga,
			kuna nii on võimalik ka mitut resurssi file reader ja teised siis neid saab
			selle semikooloniga eradada samaaegselt mitut kontrollida
			NB! vaata kas on autocloser
		 */
		try {
			FileWriter writer = new FileWriter(new File("outputResults.txt"));
			int i =1;
			while(i<=5) {
			writer.append("There are" + i + "eggs in the bird nest\n");
			i++; // väga oluline, muidu jooksutab sadu GIGASID umbe
			}
			writer.append("Chicks have hatched, there is no eggs anymore");
			// writer.flush(); // enam uutes JAVA versioonides pole seda vaja
			writer.close(); // väga tähtis kirjutada, muidu ei lõppe
		}
			catch (IOException e) { // e on siin muutuja nimi, ehk exception, mis on hiljem e.printStackTrace();
			e.printStackTrace();
			System.out.println("There is and error on writing to file outputResults.txt; " + e.getMessage());
		}
		try {
			Human human1 = new Human().setName("w"); // siin saad muuta nime
		} catch (NameTooShortException e) {
			System.out.println("Exception accured on setting human name: " + e.getMessage());
		}
	}
}

		
