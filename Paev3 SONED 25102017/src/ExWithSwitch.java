
public class ExWithSwitch {
	public static void main(String[]args) {
		String lightColor = "blinking";
		
		switch (lightColor.toLowerCase()) {
		case "green":
			System.out.println("Move");
			break;
		case "yellow":
			System.out.println("break");
			break;
		case "red":
			System.out.println("Full Stop");
			break;
		case "blinking":
			System.out.println("warning, be ready to break for full stop!");
		}
	}

}
