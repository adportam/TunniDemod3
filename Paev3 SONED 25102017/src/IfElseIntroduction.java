
public class IfElseIntroduction {

	public static void main(String[] agrs) {
		int a = 5;
		int b = 25;

		if (a >= 0 && b >= 0) {
			int vastus = a * b;
			System.out.println("a*b=" + vastus);
		}

		if (a == 5) {
			System.out.println("ei ole vaja korrutada");
		} else {
			// System.out.println("ei olnud sobivat tingimust");
		}

		/*
		 * int arv = 8 String kasArvOnKolmegaJaguv = (arv%3 == 0) ? if ( arv%3==0) {
		 * "kolmega jaguv" "Ei ole kolmega jaguv";
		 */

		String antudNimi = "Mari";
		final String MINU_NIMI = "Kati";
		boolean onMinuNimi = (antudNimi.equals(MINU_NIMI)) ? true : false;
		System.out.println("Kas minu nimi on " + antudNimi + "? Vastus: " + onMinuNimi);

		boolean isDoorOpen = false;
		String uks = (isDoorOpen) ? "uks on lahti" : "uks on kinni";
		System.out.println(uks);

	}
}
