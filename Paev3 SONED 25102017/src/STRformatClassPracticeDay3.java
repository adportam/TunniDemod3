
public class STRformatClassPracticeDay3 {

				public static void main(String[] args) {
				String stringToFormat = "%s. %s is %s years old. %<s years old is %2$s.";
				System.out.println(String.format(stringToFormat,  1, "Mari", 29));
				
				String textContainingListOfNames = "This is a list of city names: Tallinn, Tartu, Pärnu, Kuressaare, Narva";
				String textWithNamesOnly = textContainingListOfNames.split(":")[1];
				String[] arrayOfCityNames = textWithNamesOnly.split(",");
				for (String cityName : arrayOfCityNames) {
					System.out.println(cityName);
				}
				}

}
