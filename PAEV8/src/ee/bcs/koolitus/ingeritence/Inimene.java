package ee.bcs.koolitus.ingeritence;

public class Inimene {
	private String nimi;
	private int vanus;
	
	public Inimene(String nimi, int vanus) {
		this.nimi = nimi; // ojekt väärtustatakse 
		this.vanus = vanus;
	}

	public String getNimi() {
		return nimi;
	}

	public Inimene setNimi(String nimi) {
		this.nimi = nimi;
		return this;
	}

	public int getVanus() {
		return vanus;
	}

	public Inimene setVanus(int vanus) {
		this.vanus = vanus;
		return this;
	}

	@Override
	public String toString() {
		return "Inimene [nimi=" + nimi + ",vanus=" + vanus + "]";
	}
}
