package ee.bcs.koolitus.ingeritence;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main (String[] args) {;
	Inimene kati = new Inimene("Kati Kaasik", 25);
	System.out.println(kati);
	
	Opilane opilaneMati = (Opilane) new Opilane ("Mati Tamm", 7)
			/* (Opilane) - casting loob objekt Opilane, seega all olnud rida opilaneMati.setNimi("Mati Tamm");
			 * transformeerub nagu .setklass rida allpool (.set on meetod on oluline) NB!!!!! extends Opilane.java kaustas
			 * on oluline, mis võimaldab laiendada, ehk extendsssssida Opilase teistele, ehk Opilane on ka Mari Maasikas,
			 * seega JAVAle ütled, et mari on kahh Opilane
			 */
		.setKlass (1);
	System.out.println(opilaneMati);
	
	Opilane opilaneMari = new Opilane("Mari Maasikas", 10, 4);
	System.out.println(opilaneMari);
	
	Opetaja opetajaJyri = new Opetaja("Jüri Juurikas", 38);
	System.out.println(opetajaJyri);

	opetajaJyri.addNewAine("Matemaatika").addNewAine("Füüsika").addNewAine("Ajalugu");
	System.out.println(opetajaJyri);
	
	Inimene inimene = new Opetaja("Kaarel Sorts", 666); // saab teha kuna pärimisel Õpetaja on ka ju inimene
	((Opetaja)inimene).getAined();
	//System.out.println();	- hetkel ei prindi consoolis välja tee ained ja kuidas siduda nimi Kaarel Sorts ja opetajaKaarel 
	
	List<Inimene> inimesed = new ArrayList<>(); // list, array list TEGEMIST ON CASTimisega
	inimesed.add(inimene);
	inimesed.add(opetajaJyri);
	inimesed.add(opilaneMari);
	
	/* opetajaJyri.getAined().add("Ajalugu"); seda soovitakatse erijuhul, pigem kasutadaa ülalpool addNewAine meetodit
		System.out.println(opetajaJyri);
	*/
}
}
