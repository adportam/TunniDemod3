package ee.bcs.koolitus.ingeritence;

public class Opilane extends Inimene { // teise klassi puhul pead lisama kindlasti extends Inimene
	private int klass;
	// Public Opilane() {
	// (super(null, 0);
	//}
	// ei tasu teha, ega soovitata kuna hiljem võib teha
	// null-trikke, kuigi alguses see võib ära petta ja töötada

	public Opilane(String nimi, int vanus) {// kostruktor
		super(nimi, vanus);

	}
	
	public Opilane(String nimi, int vanus, int klass) { // kostruktor
		super(nimi, vanus);
		this.klass=klass;
		
		/*public Opilane(String nimi, int vanus) {// kostruktor
		super(nimi, vanus, 0);
		}
	
	public Opilane(String nimi, int vanus, int klass) { // kostruktor
		this(nimi, vanus, -1);
		this.klass=klass;
		 * 
		 * 
		 * NB!!!! Samuti saab ka
		 * public Opilane(String nimi, int vanus) {// kostruktor
		super(nimi, vanus);

	}
	
	public Opilane(String nimi, int vanus, int klass) { // kostruktor
		super(nimi, vanus);
		this.klass=klass;
		 */
	}

	public int getKlass() {
		return klass;
	}

	public Opilane setKlass(int klass) { // void vaheta suure algustähega Opilaseks
		this.klass = klass;
		// super().toString() ei saa siin kasutada
		return this;
	}

	@Override
	public String toString() {
		// return super.toString() + ".opilane [klass=" + klass + "]"; //lühike
		// koodiversioon

		return "Õpilane[nimi=" + getNimi() + ", vanus= " + getVanus() + ", klass=" + klass + "]";

	}
}
